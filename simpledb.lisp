;; This code is not original. It is from http://www.gigamonkeys.com/book/practical-a-simple-database.html
;; I'm working through this little by little and commenting for understanding.
;; I'll probably gut this later. It's a good foundation for a database.

(defvar *db* nil)

(defun make-cd (title artist rating ripped)
  ;; Makes a "dictionary" of an album's data. Title, artist, rating, and whether it is ripped
  (list :title title :artist artist :rating rating :ripped ripped))

(defun add-record (cd)
  ;; Adds a cd to the DB. Intended to be used with make-cd
  (push cd *db*))

(defun dump-db ()
  ;; Prints the database in a fancy way.
  ;; http://www.gigamonkeys.com/book/practical-a-simple-database.html contains the documentation for the
  ;; cryptic formatting.
  (dolist (cd *db*)
    (format t "~{~a:~10t~a~%~}~%" cd)))

(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))

(defun prompt-for-cd ()
  ;; Making use of make-cd, prompt-read, and y-or-n-p, it prompts the user for a CD.
  (make-cd
   (prompt-read "Title")
   (prompt-read "Artist")
   ;; Parses for the rating, looking for an integer, relaxing the input specifications,
   ;;; and returning 0 instead of nil should nothing be found.
   (or(parse-integer(prompt-read "Rating"):junk-allowed t) 0)
   (y-or-n-p "Ripped?")))

(defun add-cds ()
  (loop (add-record (prompt-for-cd))
        (if (not (y-or-n-p "Another? [y/n]: "))(return (dump-db)))))

(defun save-db (filename)
  ;; Creates or saves the database to a file.
  (with-open-file (out filename
                       ;; Output data to the file.
                       :direction :output
                       ;; If the file exists, overwrite the file.
                       :if-exists :supersede)
    (with-standard-io-syntax
      (print *db* out))))

(defun load-db (filename)
  ;; Loads a database file. This file should be created by the simpledb.lisp file
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *db* (read in)))))

;; select is a function that, based on the input function, will return a particular query in *db*
(defun select (selector-fn)
  (remove-if-not selector-fn *db*))

;; where is a function that will query the user for what they want to search for.
;; It then uses a function to find what the user is searching for.
;; This works in tandem with (select).
;; If this were to be made more user-friendly, there would be a less lispy way to call a select.
(defun where (&key title artist rating (ripped nil ripped-p))
  #'(lambda (cd)
      (and
       (if title      (equal (getf cd :title) title)t)
       (if artist     (equal (getf cd :artist) artist) t)
       (if rating     (equal (getf cd :rating) rating) t)
       (if ripped-p   (equal (getf cd :ripped) ripped) t))))

;; (load-db "~/my-cds.db")
;; Turns out you can do that! I'll update this later to automate loading, adding, saving, and such.
;; For user-friendliness!
