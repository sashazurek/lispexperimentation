(defun interest-calc ()
  (write-line "I will calculate the future value of a 10-year investment.")
  (princ "Please enter the initial principal: ")
  (setq principal (read))
  (princ "Please enter the annual interest rate: ")
  (setq apr (read))
  ;; Divide integer apr into a decimal apr appropriate for multiplication
  (setq apr (/ apr 100))
  (princ "Please enter the scholarship amount: ")
  (setq scholarship (read))
  ;; Calculate the value of the investment after 1 year, 10 times.
  (dotimes (n 10) ; (n 10) creates a loop variable, for 10 times. Removing n causes the program to break.
    (setq principal (* principal (+ 1 apr)))
    (setq principal (- principal scholarship)))
  (princ "The value in 10 years will be: ")
  (princ (float principal)))
(interest-calc)
